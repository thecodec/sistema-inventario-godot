extends Resource
class_name Item

enum Type {
	CONSUMIBLE,
	WEAPON,
	SHIELD,
	ARMOR,
	HELMET,
}

export (String) var name = ""
export (String, MULTILINE) var description = ""
export (Texture) var texture = null
export (int, 1, 999, 1) var max_quantity = 10

export (Type) var type = Type.CONSUMIBLE
