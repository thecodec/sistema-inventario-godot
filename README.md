# Sistema de inventario en Godot

Código fuente del tutorial [`Sistema de inventario en Godot`](https://www.youtube.com/playlist?list=PLr4nY7RqmgrO9kLV5MSoxje9gosgMrDsM)

### Assets usados

- Fuente: Source Code Pro (https://github.com/adobe-fonts/source-code-pro)
- GUI inventario: https://opengameart.org/content/inventory
- Cursores: https://opengameart.org/content/dwarven-cursor
- Iconos de pociones: https://opengameart.org/content/potion-bottles
- Iconos de armas y armaduras: https://opengameart.org/content/osare-weapon-icons
- Iconos roguelike: https://opengameart.org/content/roguelikerpg-items
