extends Node

func _init() -> void:
	Input.set_custom_mouse_cursor(preload("res://assets/cursors/dwarven_gauntlet_extra_8.png"), Input.CURSOR_CAN_DROP, Vector2(4, 1))
	Input.set_custom_mouse_cursor(preload("res://assets/cursors/dwarven_gauntlet_extra_6.png"), Input.CURSOR_FORBIDDEN, Vector2(4, 1))
	Input.set_custom_mouse_cursor(preload("res://assets/cursors/dwarven_gauntlet.png"), Input.CURSOR_ARROW, Vector2(4, 1))
