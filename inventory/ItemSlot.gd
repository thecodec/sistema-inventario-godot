tool
extends PanelContainer
class_name ItemSlot

export (int, -1, 1000, 1) var inventory_index:int = -1

onready var nIconTexture = find_node("IconTexture")
onready var nQuantityLabel = find_node("QuantityLabel")

var item:Item = null setget _set_item
var quantity:int = 0 setget _set_quantity

var item_type:int = -1 setget _set_item_type

var _tooltip_id:int = -1


func _ready() -> void:
	TooltipManager.connect("tooltip_outside_viewport", self, "_on_tooltip_outside_viewport")


func _set_background(item_type:int) -> void:
	var stylebox = get_stylebox("panel")
	if not stylebox is StyleBoxTexture:
		return

	stylebox = stylebox as StyleBoxTexture

	match item_type:
		Item.Type.CONSUMIBLE:
			stylebox.texture = preload("res://assets/inventory/06.png")
		Item.Type.WEAPON:
			stylebox.texture = preload("res://assets/inventory/10.png")
		Item.Type.ARMOR:
			stylebox.texture = preload("res://assets/inventory/09.png")
		Item.Type.SHIELD:
			stylebox.texture = preload("res://assets/inventory/05.png")
		Item.Type.HELMET:
			stylebox.texture = preload("res://assets/inventory/08.png")
		_:
			stylebox.texture = preload("res://assets/inventory/23.png")


func set_item(index:int, item:Item, quantity:int = 1) -> void:
	inventory_index = index
	self.item = item
	if not item:
		quantity = 0
	self.quantity = quantity


func _set_item(new_item:Item) -> void:
	item = new_item

	if not is_inside_tree():
		yield(self, "ready")

	if not item:
		nIconTexture.texture = null
	else:
		nIconTexture.texture = item.texture


func _set_quantity(new_quantity:int) -> void:
	quantity = new_quantity

	if not is_inside_tree():
		yield(self, "ready")

	if quantity <= 1:
		nQuantityLabel.visible = false
	else:
		nQuantityLabel.visible = true
		nQuantityLabel.text = "x%s" % quantity


func _set_item_type(new_value:int) -> void:
	item_type = new_value
	_set_background(item_type)


func _on_ItemSlot_mouse_entered() -> void:
	if not item:
		return

	var position = rect_global_position + Vector2(rect_size.x, 0)
	_tooltip_id = TooltipManager.create_tooltip(item.name, position)


func _on_ItemSlot_mouse_exited() -> void:
	if _tooltip_id == -1:
		return

	TooltipManager.remove_tooltip(_tooltip_id)
	_tooltip_id = -1


func _on_tooltip_outside_viewport(id:int, tooltip_rect:Rect2) -> void:
	if not _tooltip_id == id:
		return

	var position = rect_global_position - Vector2(tooltip_rect.size.x, 0)
	TooltipManager.set_tooltip_position(_tooltip_id, position)


func _get_property_list() -> Array:
	var properties = []
	properties.append({
		"name": "filter_by_type",
		"type": TYPE_BOOL,
		"usage": PROPERTY_USAGE_EDITOR,
	})

	if item_type == -1:
		properties.append({
			"name": "item_type",
			"type": TYPE_INT,
			"usage": PROPERTY_USAGE_STORAGE
		})
	else:
		properties.append({
			"name": "item_type",
			"type": TYPE_INT,
			"hint": PROPERTY_HINT_ENUM,
			"hint_string": PoolStringArray(Item.Type.keys()).join(","),
			"usage": PROPERTY_USAGE_DEFAULT
		})

	return properties


func _get(property:String):
	match property:
		"filter_by_type":
			return not item_type == -1

	return null

func _set(property:String, value) -> bool:
	match property:
		"filter_by_type":
			if not value:
				self.item_type = -1
			else:
				self.item_type = Item.Type.CONSUMIBLE

			property_list_changed_notify()
			return true
	return false
