extends Node

var pools = {

}

func _init() -> void:
	for type in Item.Type:
		pools[Item.Type.get(type)] = []

	var dir = Directory.new()
	if dir.open("res://items") == OK:
		dir.list_dir_begin(true)
		var file = dir.get_next()
		while file != "":
			if not dir.current_is_dir():
				if file.get_extension() in ["res", "tres"]:
					var item = load("res://items/%s" % file)
					if item is Item:
						pools[item.type].push_back(item)

			file = dir.get_next()

