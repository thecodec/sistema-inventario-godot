extends Node

onready var nDragAndDropForbiddenLayer = find_node("DragAndDropForbiddenLayer")

onready var nInventory = find_node("Inventory")
onready var nChest = find_node("Chest")
onready var nHotbar = find_node("Hotbar")
onready var nEquipment = find_node("Equipment")

func _ready() -> void:
	randomize()
	nDragAndDropForbiddenLayer.visible = false

	nInventory.set_inventory(_generate_inventory())
	nChest.set_inventory(_generate_inventory())
	nHotbar.set_inventory(_generate_inventory(9, false))
	nEquipment.set_inventory(_generate_inventory(3, false))


func _notification(what: int) -> void:
	match what:
		NOTIFICATION_DRAG_BEGIN:
			nDragAndDropForbiddenLayer.visible = true
		NOTIFICATION_DRAG_END:
			nDragAndDropForbiddenLayer.visible = false
			# HACK fix cursor not changing after dropping outside the inventory
			Input.warp_mouse_position(get_viewport().get_mouse_position())


func _generate_inventory(slots:int = 30, filled:bool = true) -> Inventory:

	var pools = ItemDatabase.pools.duplicate()
	var consumibles = pools[Item.Type.CONSUMIBLE]
	var others = []

	for pool_key in pools.keys():
		if not pool_key == Item.Type.CONSUMIBLE:
			others.append_array(pools[pool_key])

	var inventory = Inventory.new(slots)

	if not filled:
		return inventory

	for i in inventory.max_slots:
		if randi() % 2 == 1:
			continue

		var items = consumibles
		if randf() < 0.3:
			items = others

		items.shuffle()

		var item = items[0]
		var quantity = int(rand_range(1, item.max_quantity))
		var overflow = inventory.add_item(item, quantity, randi() % 3 == 1)
		if overflow > 0:
			print("Overflow of %s -> %s" % [item.name, overflow])

	return inventory
