tool
extends PanelContainer

export (NodePath) var items_container_path = null setget _set_items_container_path

var items_container:ItemsContainer = null

var inventory:Inventory = null

func _get_configuration_warning() -> String:
	if not items_container_path:
		return "Set an ItemsContainer node"

	var node = get_node_or_null(items_container_path)
	if not node or not node is ItemsContainer:
		return "The selected node isn't an ItemsContainer."

	return ""

func set_inventory(inventory:Inventory) -> void:
	self.inventory = inventory
	items_container.set_inventory(inventory)


func _set_items_container_path(new_value:NodePath) -> void:
	items_container_path = new_value
	update_configuration_warning()

	if not is_inside_tree():
		yield(self, "ready")

	assert(items_container_path, "The ItemsContainer NodePath is null")
	var node = get_node_or_null(items_container_path)
	assert(node and node is ItemsContainer, "The selected ItemsContainer NodePath isn't an ItemsContainer node.")
	items_container = node
