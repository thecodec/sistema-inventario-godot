extends CanvasLayer

const TOOLTIP_SCENE = preload("res://TooltipBase.tscn")

signal tooltip_outside_viewport(tooltip_id, tooltip_rect)

func create_tooltip(text:String, position:Vector2) -> int:
	var tooltip = TOOLTIP_SCENE.instance()
	var instance_id = tooltip.get_instance_id()
	add_child(tooltip)
	tooltip.set_content(text, position)
	tooltip.connect("resized", self, "_on_tooltip_resized", [instance_id])
	return instance_id

func get_tooltip_rect(id:int) -> Rect2:
	var tooltip = _get_tooltip(id)
	if not tooltip:
		return Rect2()

	tooltip = tooltip as Control

	return tooltip.get_global_rect()

func set_tooltip_position(id:int, position) -> void:
	var tooltip = _get_tooltip(id)
	if not tooltip:
		return

	tooltip.rect_global_position = position


func remove_tooltip(id:int) -> void:
	var tooltip = _get_tooltip(id)
	if not tooltip:
		return

	if tooltip in get_children():
		call_deferred("remove_child", tooltip)

	tooltip.queue_free()


func _get_tooltip(id:int) -> Control:
	var tooltip = instance_from_id(id)

	if not tooltip or not tooltip is Control:
		return null

	return tooltip as Control

func _on_tooltip_resized(id:int) -> void:
	var tooltip = _get_tooltip(id)
	if not tooltip:
		return

	var viewport_rect = get_viewport().get_visible_rect()
	var tooltip_rect = tooltip.get_global_rect()
	if not viewport_rect.encloses(tooltip_rect):
		emit_signal("tooltip_outside_viewport", id, tooltip_rect)


