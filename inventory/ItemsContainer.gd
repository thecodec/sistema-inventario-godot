tool
extends PanelContainer
class_name ItemsContainer

const ITEM_SLOT_SCENE = preload("res://inventory/ItemSlot.tscn")

var slot_container:Control = null
var inventory:Inventory = null

func _ready() -> void:
	if get_child_count() > 0:
		var child = get_child(0)
		if child is Control:
			slot_container = child

	assert(slot_container, "No control added to contain the inventory slots.")


func _get_configuration_warning() -> String:
	var show_warning = false
	if get_child_count() <= 0:
		show_warning = true

	if not show_warning:
		var child = get_child(0)
		show_warning = not child or not child is Control

	if show_warning:
		return "The first child of this node needs to be a control node to contain the inventory slots"

	return ""


func _notification(what: int) -> void:
	if not inventory:
		return

	match what:
		NOTIFICATION_DRAG_END:
			for child in slot_container.get_children():
				if not child is ItemSlot:
					continue

				var stack = inventory.get_item_stack(child.inventory_index)
				child.set_item(child.inventory_index, stack.item, stack.quantity)


func set_inventory(inventory:Inventory) -> void:
	self.inventory = inventory

	inventory.connect("slot_changed", self, "_on_inventory_slot_changed")

	for i in inventory.max_slots:
		var inventory_index = i
		var stack = inventory.get_item_stack(inventory_index)
		var slot:ItemSlot = null
		if i < slot_container.get_child_count():
			var child = slot_container.get_child(i)
			if child and child is ItemSlot:
				slot = child as ItemSlot
				if slot.inventory_index > -1:
					inventory_index = slot.inventory_index

		if not slot:
			slot = ITEM_SLOT_SCENE.instance()
			slot_container.add_child(slot)

		slot.set_drag_forwarding(self)
		slot.set_item(inventory_index, stack.item, stack.quantity)
		slot.connect("gui_input", self, "_on_slot_gui_input", [slot])


func can_drop_data_fw(position: Vector2, data, other_control:Control) -> bool:
	if not other_control is ItemSlot:
		return false

	if not data.has("slot"):
		return false

	var from_control = data.slot as ItemSlot
	var to_control = other_control as ItemSlot

	var from_item = data.inventory.get_item_stack(from_control.inventory_index).item
	var to_item = inventory.get_item_stack(to_control.inventory_index).item

	# Don't let drop data if the split mode is active and the to slot
	# doesn't have the same item
	if data.get("split", false):
		if to_item and not to_item == from_item:
			return false

	# Don't let drop data if the slot has a type assigned and the type
	# of the item don't match
	if not to_control.item_type == -1 and not to_control.item_type == from_item.type:
		return false

	# Don't let drop data if the origin slot has a type assigned and
	# the destination item type don't match
	if to_item:
		if not from_control.item_type == -1 and not from_control.item_type == to_item.type:
			return false

	return true


func get_drag_data_fw(position: Vector2, other_control:Control):
	if not other_control is ItemSlot or not other_control.item:
		return null

	other_control = other_control as ItemSlot

	set_drag_preview(create_preview(position, other_control.item, other_control.quantity))

	other_control.set_item(other_control.inventory_index, null, 0)
	other_control.emit_signal("mouse_exited")

	return {"slot": other_control, "inventory": inventory, "split": false}


func drop_data_fw(position: Vector2, data, other_control:Control) -> void:
	if not other_control is ItemSlot:
		return

	var from_inventory:Inventory = data.get("inventory", null)
	var to_inventory = inventory

	var from_index = data.slot.inventory_index
	var to_index = other_control.inventory_index

	other_control.emit_signal("mouse_exited")

	var from_stack = data.get("from_stack", null)
	var to_stack = data.get("to_stack", null)
	Inventory.swap_slots_from_inventories(from_inventory, from_index, to_inventory, to_index, from_stack, to_stack)

	yield(get_tree(), "idle_frame")
	other_control.emit_signal("mouse_entered")



func create_preview(position:Vector2, item:Item, quantity:int) -> Control:
	var preview = ITEM_SLOT_SCENE.instance()
	preview.set_item(-1, item, quantity)
	preview.modulate.a = 0.6

	var n2d = Node2D.new()
	n2d.z_index = 4096
	n2d.add_child(preview)

	var pivot = Control.new()
	pivot.add_child(n2d)
	preview.rect_position = -position

	return pivot


func _on_inventory_slot_changed(index:int, old_content:ItemStack, new_content:ItemStack) -> void:
	for child in slot_container.get_children():
		if not child is ItemSlot:
			continue

		child = child as ItemSlot

		if child.inventory_index == index:
			child.set_item(index, new_content.item, new_content.quantity)
			break


func _input(event: InputEvent) -> void:
	# If dragging is happening and the event is a mouse event then:
	# - If the event is a mouse motion and button mask left isn't in its button mask
	#   we accept the current event, duplicate it, add that mask to the event
	#   and send that modified event again.
	# - If the event is a mouse button, its button index is the right click and
	#   is not pressed (right click is released) we clean up the dragging start metadata
	#   just in case and a button left release event so godot know to finish the drag event.

	if not get_viewport().gui_is_dragging():
		return

	if not event is InputEventMouse:
		return

	if event is InputEventMouseMotion:
		event = event as InputEventMouseMotion
		if not event.button_mask & BUTTON_MASK_LEFT:
			accept_event()

			event = event.duplicate()
			event.button_mask |= BUTTON_MASK_LEFT
			Input.parse_input_event(event)

	if event is InputEventMouseButton:
		event = event as InputEventMouseButton
		if not event.pressed and event.button_index == BUTTON_MASK_RIGHT:
			set_meta("__dragging_start", null)

			event = event.duplicate()
			event.button_index = BUTTON_LEFT
			event.button_mask = BUTTON_MASK_LEFT
			Input.parse_input_event(event)


func _on_slot_gui_input(event:InputEvent, slot:ItemSlot) -> void:
	# If the slot has an item and it receives a mouse event then:
	# - If the event is the pressed right click button we set the drag start point
	#   to the position of the event.
	# - If the event is a motion event, we have set the drag start point
	#   and the button mask has a right button in it
	#   we check the distance between the drag start point and the current event
	#   position. If it's bigger than 10 we check if need to split, force a drag
	#   and propagate a drag begin notification.

	if not slot.item:
		return

	if not event is InputEventMouse:
		return

	if event is InputEventMouseButton:
		event = event as InputEventMouseButton
		if event.button_index == BUTTON_RIGHT and event.pressed:
			set_meta("__dragging_start", event.position)

	if event is InputEventMouseMotion:
		event = event as InputEventMouseMotion
		if has_meta("__dragging_start") and event.button_mask & BUTTON_MASK_RIGHT:
			var drag_start:Vector2  = get_meta("__dragging_start")
			var diff:Vector2 = drag_start - event.position
			if diff.length() > 10:
				var drag_data = {"slot": slot, "inventory": inventory}
				var stacks = inventory.split_item_stack(slot.inventory_index, 0.5)

				var preview = null
				if stacks.empty():
					drag_data["split"] = false
					slot.set_item(slot.inventory_index, null, 0)
					var stack = inventory.get_item_stack(slot.inventory_index)
					preview = create_preview(drag_start, stack.item, stack.quantity)
				else:
					var min_stack = stacks.get("min_stack")
					var max_stack = stacks.get("max_stack")
					drag_data["split"] = true
					drag_data["from_stack"] = min_stack
					drag_data["to_stack"] = max_stack
					slot.set_item(slot.inventory_index, min_stack.item, min_stack.quantity)
					preview = create_preview(drag_start, max_stack.item, max_stack.quantity)

				force_drag(drag_data, preview)

				get_viewport().propagate_notification(NOTIFICATION_DRAG_BEGIN)
				set_meta("__dragging_start", null)
