extends PanelContainer

onready var nContentLabel = find_node("ContentLabel")

func set_content(text:String, position:Vector2) -> void:
	if not is_inside_tree():
		yield(self, "ready")

	nContentLabel.text = text
	rect_global_position = position
	rect_size = Vector2.ZERO

